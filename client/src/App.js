import React, { Component } from 'react';
import './App.css';

class App extends Component {
    // Initialize state
    state = { books: [] ,appInformation: {}}

    // Fetch passwords after first mount
    componentDidMount() {
        this.getBooks();
        this.getAppInformation();
    }

    getAppInformation = () => {
        fetch('/api/info')
            .then(res => res.json())
            .then(appInformation => this.setState({ appInformation }));
    }
    getBooks = () => {
        // Get the passwords and store them in state
        fetch('/api/db')
            .then(res => res.json())
            .then(books => this.setState({ books }));
    }

    renderAppInformation(appInformation){
        return(
          <div class="AppInformation">
              <p>key: {appInformation.key}</p>
              <p>iv: {appInformation.iv}</p>
              <p>value: {appInformation.value}</p>

          </div>
        );
    }
    renderNoInformation(){
        return(
          <p>There is no app information</p>
        );
    }
    render() {
        const { books,appInformation } = this.state;
        // console.log(books);
        return (
            <div className="App">
            <header id="header">
                <h1>App information</h1>
                <a href="https://gitlab.com/Sophein/epf-ism-bouras-sophein-lab2" class="fa fa-gitlab" target="_blank"></a>
            </header>
                {appInformation && this.renderAppInformation(appInformation) || this.renderNoInformation()}
                <h1>{books.length} books.</h1>
                {/* Render the passwords if we have them */}
                {books.length ? (
                    <div>


                        <ul className="books">
                            {/*
                Generally it's bad to use "index" as a key.
                It's ok for this example because there will always
                be the same number of passwords, and they never
                change positions in the array.
              */}
                            {books.map((book, index) =>
                                <li style={{color: '#424f55'}}  key={index}>
                                    <a class="thumbnail" src={book.thumbnailUrl} data-position="left center"><img src={book.thumbnailUrl} alt="Dust Cover" height="150" width="150"/></a>
							        <h2>{book.title}</h2>
							        <p>{book.shortDescription}</p>
						        </li>
                            )}
                        </ul>
                        
                        <button
                            className="more"
                            onClick={this.getBooks}>
                            Get More
                        </button>
                    </div>
                ) : (
                    // Render a helpful message otherwise
                    <div>
                        <h1>No books :(</h1>
                        <button
                            className="more"
                            onClick={this.getBooks}>
                            Try Again?
                        </button>
                    </div>
                )}
            </div>
        );
    }
}

export default App;
